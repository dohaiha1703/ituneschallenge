package com.hado.ituneschallenge.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.hado.ituneschallenge.models.entity.Movie

open class MovieListConverter {
    @TypeConverter
    fun fromString(value: String): List<Movie>? {
        val listType = object : TypeToken<List<Movie>>() {}.type
        return Gson().fromJson<List<Movie>>(value, listType)
    }

    @TypeConverter
    fun fromList(list: List<Movie>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}
