package com.hado.ituneschallenge.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.hado.ituneschallenge.models.entity.Movie
import com.hado.ituneschallenge.room.converters.MovieListConverter

@Database(
    entities = [(Movie::class)],
    version = 1, exportSchema = false
)
@TypeConverters(value = [(MovieListConverter::class)])
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}
