package com.hado.ituneschallenge.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.hado.ituneschallenge.models.entity.Movie

@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovieList(movies: List<Movie>)

    @Update
    fun updateMovie(movie: Movie)

    @Query("SELECT * FROM Movie")
    fun getMovieList(): LiveData<List<Movie>>
}
