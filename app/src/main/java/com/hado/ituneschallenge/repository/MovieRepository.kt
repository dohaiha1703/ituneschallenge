package com.hado.ituneschallenge.repository

import androidx.lifecycle.LiveData
import com.hado.ituneschallenge.api.ApiResponse
import com.hado.ituneschallenge.api.MovieService
import com.hado.ituneschallenge.compose.AppExecutors
import com.hado.ituneschallenge.models.Resource
import com.hado.ituneschallenge.models.entity.Movie
import com.hado.ituneschallenge.models.network.ItunesMoviesResponse
import com.hado.ituneschallenge.room.MovieDao
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieRepository @Inject constructor(
    val service: MovieService,
    val movieDao: MovieDao
) {

    init {
        Timber.d("Injection MovieRepository")
    }

    fun loadVideoList(): LiveData<Resource<List<Movie>>> {
        return object :
            NetworkBoundRepository<List<Movie>, ItunesMoviesResponse>() {
            override fun saveFetchData(items: ItunesMoviesResponse) {
                movieDao.insertMovieList(items.movies)
            }

            override fun shouldFetch(data: List<Movie>?): Boolean {
                return data == null || data.isEmpty()
            }

            override fun loadFromDb(): LiveData<List<Movie>> {
                return movieDao.getMovieList()
            }

            override fun fetchService(): LiveData<ApiResponse<ItunesMoviesResponse>> {
                return service.fetchMovies()
            }

            override fun onFetchFailed(message: String?) {
                Timber.d("onFetchFailed : $message")
            }
        }.asLiveData()
    }

    fun updateMovie(movie: Movie) {
        AppExecutors.diskIO().execute {
            movieDao.updateMovie(movie)
        }
    }
}
