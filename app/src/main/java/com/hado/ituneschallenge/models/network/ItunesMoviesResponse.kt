package com.hado.ituneschallenge.models.network

import com.google.gson.annotations.SerializedName
import com.hado.ituneschallenge.models.NetworkResponseModel
import com.hado.ituneschallenge.models.entity.Movie

data class ItunesMoviesResponse(
    @SerializedName("resultCount")
    val resultCount: Int,
    @SerializedName("results")
    val movies: List<Movie>
): NetworkResponseModel