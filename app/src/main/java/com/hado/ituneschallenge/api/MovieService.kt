package com.hado.ituneschallenge.api

import androidx.lifecycle.LiveData
import com.hado.ituneschallenge.models.network.ItunesMoviesResponse
import retrofit2.http.GET

interface MovieService {
    @GET("search?term=star&country=au&media=movie&;all")
    fun fetchMovies(): LiveData<ApiResponse<ItunesMoviesResponse>>
}
