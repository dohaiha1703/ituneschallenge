package com.hado.ituneschallenge.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hado.ituneschallenge.R
import com.hado.ituneschallenge.models.entity.Movie

class GenreListAdapter(
    private val context: Context,
    private val movies: List<Movie>,
    private val noDataListener: (Boolean) -> Unit
) : RecyclerView.Adapter<GenreListAdapter.GenreListViewHolder>() {

    private var moviesToShow = movies
    private var genreList = getGenreList()
    var onClickFavorite: ((Movie) -> Unit)? = null
    var onClickMovieItem: ((Movie, Int, MovieListAdapter) -> Unit)? = null

    class GenreListViewHolder constructor(
        val view: View
    ) : RecyclerView.ViewHolder(view) {
        var tvGenre = view.findViewById<View>(R.id.tvGenre) as TextView
        var rvMovies = view.findViewById<View>(R.id.rvMovies) as RecyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreListViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_genre, parent, false)
        return GenreListViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return genreList.size
    }

    override fun onBindViewHolder(holder: GenreListViewHolder, position: Int) {
        val genre = genreList[position]
        val movieAdapter = MovieListAdapter(
            context,
            getMovieListByGenre(genre),
            onClickFavorite!!,
            onClickMovieItem!!
        )
        holder.rvMovies.adapter = movieAdapter
        holder.tvGenre.text = genre
    }

    private fun getGenreList(): List<String> {
        val genreList = mutableListOf<String>()
        moviesToShow.forEach { movie ->
            if (!genreList.contains(movie.primaryGenreName)) {
                //primaryGenreName = null -> No Genre
                genreList.add(
                    movie.primaryGenreName ?: "No Genre"
                )
            }
        }
        return genreList
    }

    private fun getMovieListByGenre(genre: String): MutableList<Movie> {
        return if (genre == "No Genre") { //No Genre -> primaryGenreName = null
            moviesToShow.filter { it.primaryGenreName == null }.toMutableList()
        } else {
            moviesToShow.filter { it.primaryGenreName == genre }.toMutableList()
        }
    }

    fun onSearchTyping(keyword: String?) {
        moviesToShow = if (keyword.isNullOrEmpty()) {
            movies
        } else {
            movies.filter { it.trackName?.contains(keyword, ignoreCase = true) == true }
        }
        genreList = getGenreList()
        notifyDataSetChanged()
        noDataListener.invoke(moviesToShow.isEmpty())
    }

    fun updateMovieItem(movie: Movie?, position: Int, adapter: MovieListAdapter) {
        movie?.let {
            adapter.updateMovieItem(movie, position)
        }
    }

}