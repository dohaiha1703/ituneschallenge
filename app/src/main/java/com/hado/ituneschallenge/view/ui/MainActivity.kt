package com.hado.ituneschallenge.view.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.addTextChangedListener
import com.hado.ituneschallenge.compose.ViewModelActivity
import com.hado.ituneschallenge.databinding.ActivityMainBinding
import com.hado.ituneschallenge.models.Resource
import com.hado.ituneschallenge.models.Status
import com.hado.ituneschallenge.models.entity.Movie
import com.hado.ituneschallenge.view.adapter.GenreListAdapter
import com.hado.ituneschallenge.view.adapter.MovieListAdapter
import com.hado.ituneschallenge.viewmodel.MovieViewModel

class MainActivity : ViewModelActivity() {
    private val viewModel: MovieViewModel by injectViewModels()
    private lateinit var binding: ActivityMainBinding
    private var selectedMovie: Movie? = null
    private var adapter: GenreListAdapter? = null
    private var selectedPosition = -1
    private var selectedMovieListAdapter: MovieListAdapter? = null

    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data
                selectedMovie = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    intent?.getParcelableExtra(MovieDetailActivity.MOVIE_KEY, Movie::class.java)
                } else {
                    intent?.getParcelableExtra(MovieDetailActivity.MOVIE_KEY)
                }

            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initLiveData()
        initViewListener()
    }

    private fun initLiveData() {
        viewModel.movieListLiveData.observe(this) { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    if (adapter != null) {
                        updateMovieIfNeed()
                    } else {
                        initMoviesAdapter(resource)
                    }
                }
                Status.ERROR -> {

                }
                Status.LOADING -> {

                }
            }
        }
    }

    private fun updateMovieIfNeed() {
        selectedMovie?.let {
            adapter!!.updateMovieItem(
                it,
                selectedPosition,
                selectedMovieListAdapter!!
            )
        }
    }

    private fun initMoviesAdapter(resource: Resource<List<Movie>>) {
        adapter = GenreListAdapter(this, resource.data ?: listOf()) { isNoData ->
            if (isNoData) {
                binding.rvGenre.visibility = View.GONE
                binding.tvNoData.visibility = View.VISIBLE
            } else {
                binding.rvGenre.visibility = View.VISIBLE
                binding.tvNoData.visibility = View.GONE
            }
        }
        adapter!!.onClickFavorite = { movie ->
            viewModel.updateMovie(movie)
        }
        adapter!!.onClickMovieItem = { movie, position, movieListAdapter ->
            selectedMovie = movie
            selectedPosition = position
            selectedMovieListAdapter = movieListAdapter

            startForResult.launch(
                Intent(
                    this,
                    MovieDetailActivity::class.java
                ).apply {
                    putExtra(MovieDetailActivity.MOVIE_KEY, movie)
                })
        }
        binding.rvGenre.adapter = adapter
    }

    private fun initViewListener() {
        binding.edtSearch.addTextChangedListener {
            adapter?.onSearchTyping(it.toString())
            if (it.toString().isEmpty()) {
                binding.ivCloseIcon.visibility = View.INVISIBLE
                binding.ivCloseIcon.isClickable = false
                binding.ivCloseIcon.isEnabled = false
            } else {
                binding.ivCloseIcon.visibility = View.VISIBLE
                binding.ivCloseIcon.isClickable = true
                binding.ivCloseIcon.isEnabled = true
            }
        }

        binding.edtSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard()
                binding.edtSearch.clearFocus()
            }
            false
        }

        binding.ivCloseIcon.setOnClickListener {
            binding.edtSearch.setText("")
            binding.edtSearch.clearFocus()
            hideKeyboard()
        }
    }

    private fun hideKeyboard() {
        this.currentFocus?.let { view ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}