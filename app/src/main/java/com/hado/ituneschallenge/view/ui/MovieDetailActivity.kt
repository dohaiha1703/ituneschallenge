package com.hado.ituneschallenge.view.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.hado.ituneschallenge.R
import com.hado.ituneschallenge.compose.ViewModelActivity
import com.hado.ituneschallenge.databinding.ActivityDetailBinding
import com.hado.ituneschallenge.models.entity.Movie
import com.hado.ituneschallenge.viewmodel.MovieViewModel

class MovieDetailActivity : ViewModelActivity() {

    companion object {
        const val MOVIE_KEY = "MOVIE_KEY"
    }

    private val viewModel: MovieViewModel by injectViewModels()
    private lateinit var binding: ActivityDetailBinding
    private var movie: Movie? = null

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        movie = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getParcelableExtra(MOVIE_KEY, Movie::class.java)
        } else {
            intent.getParcelableExtra(MOVIE_KEY)
        }

        movie?.let {
            initMovieDetail(it)
        }
    }

    private fun initMovieDetail(movie: Movie) {
        binding.tvTitle.text = movie.trackName
        binding.tvArtistName.text = "Artist Name: ${movie.artistName}"
        binding.tvGenre.text = movie.primaryGenreName
        binding.tvLongDescription.text = movie.longDescription
        binding.tvPrice.text = "${movie.currency} ${movie.trackPrice}"

        val options: RequestOptions = RequestOptions()
            .fitCenter()
            .placeholder(R.drawable.ic_place_holder)
            .error(R.drawable.ic_place_holder)

        Glide.with(this).load(movie.artworkUrl100).apply(options).into(binding.ivPoster)

        binding.ivFavorite.setImageResource(if (movie.isFavorite) R.drawable.ic_favorite_24 else R.drawable.ic_favorite_border_24)
        binding.ivFavorite.setOnClickListener {
            handleClickFavorite(movie)
        }
    }

    private fun handleClickFavorite(movie: Movie) {
        movie.isFavorite = !movie.isFavorite
        viewModel.updateMovie(movie)
        binding.ivFavorite.setImageResource(if (movie.isFavorite) R.drawable.ic_favorite_24 else R.drawable.ic_favorite_border_24)
        setResult(RESULT_OK, Intent().apply {
            putExtra(MOVIE_KEY, movie)
        })
    }
}
