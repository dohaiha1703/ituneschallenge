package com.hado.ituneschallenge.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.hado.ituneschallenge.R
import com.hado.ituneschallenge.models.entity.Movie


class MovieListAdapter(
    private val context: Context,
    private val movies: MutableList<Movie>,
    private val onClickFavorite: (Movie) -> Unit,
    private val onClickItem: (Movie, Int, MovieListAdapter) -> Unit
) : RecyclerView.Adapter<MovieListAdapter.MovieListViewHolder>() {

    class MovieListViewHolder constructor(
        val view: View
    ) : RecyclerView.ViewHolder(view) {
        var tvTitle = view.findViewById<View>(R.id.tvTitle) as TextView
        var tvPrice = view.findViewById<View>(R.id.tvPrice) as TextView
        var tvRating = view.findViewById<View>(R.id.tvRating) as TextView
        var ivPoster = view.findViewById<View>(R.id.ivPoster) as ImageView
        var ivFavorite = view.findViewById<View>(R.id.ivFavorite) as ImageView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return MovieListViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MovieListViewHolder, position: Int) {
        val movie = movies[position]
        holder.tvTitle.text = movie.trackName
        holder.tvPrice.text = "${movie.currency} ${movie.trackPrice}"
        holder.tvRating.text = "Rating: ${movie.contentAdvisoryRating}"

        val options: RequestOptions =
            RequestOptions().fitCenter().placeholder(R.drawable.ic_place_holder)
                .error(R.drawable.ic_place_holder)

        Glide.with(context).load(movie.artworkUrl100).apply(options).into(holder.ivPoster)

        holder.view.setOnClickListener {
            onClickItem.invoke(movie, position, this)
        }

        holder.ivFavorite.setImageResource(if (movie.isFavorite) R.drawable.ic_favorite_24 else R.drawable.ic_favorite_border_24)
        holder.ivFavorite.setOnClickListener {
            handleClickFavorite(movie, position)
        }
    }

    private fun handleClickFavorite(
        movie: Movie,
        position: Int
    ) {
        movie.isFavorite = !movie.isFavorite
        onClickFavorite.invoke(movie)
        notifyItemChanged(position)
    }

    fun updateMovieItem(movie: Movie, position: Int) {
        movies[position] = movie
        notifyItemChanged(position)
    }


}
