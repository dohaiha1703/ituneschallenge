package com.hado.ituneschallenge.di

import com.hado.ituneschallenge.di.annotations.ActivityScope
import com.hado.ituneschallenge.view.ui.MovieDetailActivity
import com.hado.ituneschallenge.view.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector()
    internal abstract fun contributeMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun contributeMovieDetailActivity(): MovieDetailActivity
}
