package com.hado.ituneschallenge.di

import com.hado.ituneschallenge.compose.ViewModelActivity
import com.hado.ituneschallenge.di.annotations.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ComposeModule {

    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun contributeViewModelActivity(): ViewModelActivity
}
