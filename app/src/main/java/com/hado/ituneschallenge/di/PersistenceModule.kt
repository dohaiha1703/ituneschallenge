package com.hado.ituneschallenge.di

import android.app.Application
import androidx.annotation.NonNull
import androidx.room.Room
import com.hado.ituneschallenge.room.AppDatabase
import com.hado.ituneschallenge.room.MovieDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PersistenceModule {

    @Provides
    @Singleton
    fun provideDatabase(@NonNull application: Application): AppDatabase {
        return Room
            .databaseBuilder(application, AppDatabase::class.java, "ItunesMovies.db")
            .build()
    }

    @Provides
    @Singleton
    fun provideMovieDao(@NonNull database: AppDatabase): MovieDao {
        return database.movieDao()
    }
}
