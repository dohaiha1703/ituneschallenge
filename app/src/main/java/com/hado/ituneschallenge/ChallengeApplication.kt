package com.hado.ituneschallenge

import com.hado.ituneschallenge.di.DaggerAppComponent
import dagger.android.DaggerApplication
import timber.log.Timber

class ChallengeApplication : DaggerApplication() {

    private val appComponent = DaggerAppComponent.factory().create(this)

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun applicationInjector() = appComponent
}