package com.hado.ituneschallenge.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.hado.ituneschallenge.models.Resource
import com.hado.ituneschallenge.models.entity.Movie
import com.hado.ituneschallenge.repository.MovieRepository
import timber.log.Timber
import javax.inject.Inject

class MovieViewModel @Inject constructor(
    private val repository: MovieRepository
) : ViewModel() {

    val movieListLiveData: LiveData<Resource<List<Movie>>>

    init {
        Timber.d("Injection MovieDetailViewModel")
        this.movieListLiveData = repository.loadVideoList()
    }

    fun updateMovie(movie: Movie) {
        repository.updateMovie(movie)
    }
}
